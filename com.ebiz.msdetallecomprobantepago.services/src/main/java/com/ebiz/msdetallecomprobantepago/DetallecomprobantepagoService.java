package com.ebiz.msdetallecomprobantepago;

import com.ebiz.msdetallecomprobantepago.Comprobantepago;

public interface DetallecomprobantepagoService {
	Comprobantepago detalleComprobantepago(int id);
}
