package com.ebiz.msdetallecomprobantepago.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ebiz.msdetallecomprobantepago.Comprobantepago;
import com.ebiz.msdetallecomprobantepago.DetallecomprobantepagoRepository;
import com.ebiz.msdetallecomprobantepago.DetallecomprobantepagoService;

@Service
public class DetallecomprobantepagoServiceImpl implements DetallecomprobantepagoService{
	
	@Autowired
	private DetallecomprobantepagoRepository detallecomprobantepagoRepository;
	
	public Comprobantepago detalleComprobantepago(int id) {
		return detallecomprobantepagoRepository.detalleComprobantepago(id);
	}
}
