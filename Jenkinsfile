#!groovy

node {   
	def dNameMS = "msdetallecomprobantepago"
	def dNameBuildNumber = dNameMS + ":" + "${BUILD_NUMBER}"
	def dNamePushed = "ebizacrqa.azurecr.io/" + dNameBuildNumber	
	def dNamePushedLatest = "ebizacrqa.azurecr.io/" + dNameMS + ":latest"
					  
	def app	
	def mvnHome
    def sonarqubeScannerHome     
   
	stage('Preparación') { 
	   checkout scm
	   mvnHome = tool name: 'M3', type: 'maven'  
	   sonarqubeScannerHome = tool name: 'SQ', type: 'hudson.plugins.sonar.SonarRunnerInstallation'	   
	}

	stage('Build') {       
	   sh "mvn clean package -Pswarm"		   	   
	}
	  
	stage('Prueba Unitaria') {
	  step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
	}

	stage('Cobertura') {
	  step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
	}

	stage('SonarQube') {
	  withSonarQubeEnv('SonarQube') {		
		sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar'
	  }
	}
	  
	stage('Result') {
	  archiveArtifacts artifacts: '**/target/*.jar, **/target/*.war', fingerprint:true, onlyIfSuccessful:true
	}      
		 
	stage('DOCKER') {
		
		/*DOCKER: Clone repository */
		/* Let's make sure we have the repository cloned to our workspace */
		checkout scm
		
		/*DOCKER: Build image*/
		/* This builds the actual image; synonymous to
		 * docker build on the command line*/
		app = docker.build(dNameBuildNumber)
		
		/*DOCKER: Test image*/
		app.inside {
			sh 'echo "Tests passed"'
		}
		
		/*DOCKER: Push image*/
		 /* Finally, we'll push the image with two tags::
		 * First, the incremental build number from Jenkins
		 * Second, the 'latest' tag.
		 * Pushing multiple tags is cheap, as all the layers are reused. */
		docker.withRegistry('https://ebizacrqa.azurecr.io/v2/', 'afafedba-03b2-45d0-89be-4226bbf79819') {		
			app.push("${BUILD_NUMBER}")
			app.push("latest")
	   }
		
	}
     
	
    stage('KUBERNETES') {		
		withCredentials([usernamePassword(credentialsId: 'azure-service-principal',
						usernameVariable: 'AZURE_PRINCIPAL_ID', passwordVariable: 'AZURE_PRINCIPAL_PASSWORD')]) {
			withCredentials([[$class: 'FileBinding', variable: 'PVT_KEY_FILE', credentialsId: 'kubernetes']]) {
				withCredentials([[$class: 'StringBinding', variable: 'SHH_KEYGEN', credentialsId: 'shh-keygen']]) {
					sh """
						rm -rf ~/.ssh
						mkdir ~/.ssh
										
						cat $PVT_KEY_FILE > ~/.ssh/id_rsa
						chmod 0600 ~/.ssh/id_rsa

						az login -u $AZURE_PRINCIPAL_ID -p $AZURE_PRINCIPAL_PASSWORD

						ssh-keygen -p -P $SHH_KEYGEN -N "" -f ~/.ssh/id_rsa											
						az acs kubernetes get-credentials --resource-group=RG_QA_B2MD --name=ebizacsqa 
						
						az acs kubernetes install-cli							
						kubectl create -f k8s-service.yml				
					"""		
				}					
			}
		}
	}
		
	stage('Kubernetes update') {
		sh """				
			kubectl set image deployment/msdetallecompro msdetallecompro=$dNamePushedLatest
		"""
	}
			
}

 