package com.ebiz.msdetallecomprobantepago;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Comprobantepago {
	
	@JsonProperty("idcomprobantepago")
	private int idcomprobantepago;

	@JsonProperty("num_comprobantepago")
	private String num_comprobantepago;
	
	@JsonProperty("idusuarioproveedor")
	private int idusuarioproveedor;
	
	@JsonProperty("idorganizacioncompradora")
	private int idorganizacioncompradora;
	
	@JsonProperty("idorganizacionproveedora")
	private int idorganizacionproveedora;
	
	@JsonProperty("rucproveedor")
	private String rucproveedor;
	
	@JsonProperty("ruccomprador")
	private String ruccomprador;
	
	@JsonProperty("idestado")
	private int idestado;
	
	@JsonProperty("idmoneda")
	private int idmoneda;
	
	@JsonProperty("estado_comprobantepago")
	private String estado_comprobantepago;
	
	@JsonProperty("flag_plazopago")
	private String flag_plazopago;
	
	@JsonProperty("flag_registroeliminado")
	private String flag_registroeliminado;
	
	@JsonProperty("flag_origencomprobantepago")
	private String flag_origencomprobantepago;
	
	@JsonProperty("flag_origencreacion")
	private String flag_origencreacion;
	
	@JsonProperty("in_idtipodocumento")
	private int in_idtipodocumento;
	
	@JsonProperty("ch_idtipodocumento")
	private String ch_idtipodocumento;
	
	@JsonProperty("idguia")
	private int idguia;
	
	@JsonProperty("idoc")
	private int idoc;
	
	@JsonProperty("idusuariocreacion")
	private int idusuariocreacion;
	
	@JsonProperty("idusuariomodificacion")
	private int idusuariomodificacion;
	
	@JsonProperty("idorganizacioncreacion")
	private int idorganizacioncreacion;
	
	@JsonProperty("idorganizacionmodificacion")
	private int idorganizacionmodificacion;
	
	@JsonProperty("razonsocialproveedora")
	private String razonsocialproveedora;
	
	@JsonProperty("razonsocialcompradora")
	private String razonsocialcompradora;
	
	@JsonProperty("monedacomprobantepago")
	private String monedacomprobantepago;
	
	
	@JsonProperty("flagenvioguia")
	private String flagenvioguia;
	
	@JsonProperty("fechaprogpagocomprobante")
	private String fechaprogpagocomprobante;
	
	@JsonProperty("fechapagocomprobante")
	private String fechapagocomprobante;
	
	@JsonProperty("fechacreacion")
	private String fechacreacion;
	
	@JsonProperty("fecharegistro")
	private String fecharegistro;
	
	@JsonProperty("fechaemision")
	private String fechaemision;
	
	@JsonProperty("fecharecepcioncomprobante")
	private String fecharecepcioncomprobante;
	
	@JsonProperty("fechavencimiento")
	private String fechavencimiento;
	
	@JsonProperty("fechaenvio")
	private String fechaenvio;
	
	@JsonProperty("fechacambioestado")
	private String fechacambioestado;
	
	@JsonProperty("obscomprobante")
	private String obscomprobante;
	
	@JsonProperty("obspagocomprobante")
	private String obspagocomprobante;
	
	@JsonProperty("impuesto1")
	private double impuesto1;
	
	@JsonProperty("impuesto2")
	private double impuesto2;
	
	@JsonProperty("impuesto3")
	private double impuesto3;
	
	@JsonProperty("descuento")
	private double descuento;
	
	@JsonProperty("importereferencial")
	private double importereferencial;
	
	
	@JsonProperty("subtotalcomprobante")
	private double subtotalcomprobante;
	
	@JsonProperty("totalcomprobante")
	private double totalcomprobante;
	
	@JsonProperty("campo1")
	private String campo1;
	
	@JsonProperty("campo2")
	private String campo2;
	
	@JsonProperty("condicionpago")
	private String condicionpago;
	
	@JsonProperty("tiempoplazo")
	private String tiempoplazo;
	
	@JsonProperty("tipo_operacion")
	private String tipo_operacion;
	
	@JsonProperty("documentopago")
	private String documentopago;
	
	@JsonProperty("formapago")
	private String formapago;
	
	@JsonProperty("tipocomprobante")
	private String tipocomprobante;
	
	@JsonProperty("estadocomprobante")
	private String estadocomprobante;
	
	@JsonProperty("idestadocomprobante")
	private int idestadocomprobante;
	
	@JsonProperty("version")
	private int version;
	
	@JsonProperty("idusuariocomprador")
	private int idusuariocomprador;
	
	@JsonProperty("numoc")
	private String numoc;
	
	@JsonProperty("numguia")
	private String numguia;
	
	@JsonProperty("montocomprobante")
	private String montocomprobante;
	
	@JsonProperty("logo")
	private String logo;
	
	@JsonProperty("firma")
	private String firma;
	
	@JsonProperty("pagotipodocumento")
	private String pagotipodocumento;
	
	@JsonProperty("pagonrodocumento")
	private String pagonrodocumento;
	
	@JsonProperty("pagomoneda")
	private String pagomoneda;
	
	@JsonProperty("pagomontopagado")
	private double pagomontopagado;
	
	@JsonProperty("pagobanco")
	private String pagobanco;
	
	@JsonProperty("dctotipodocumento")
	private String dctotipodocumento;
	
	@JsonProperty("dctonrodocumento")
	private String dctonrodocumento;
	
	@JsonProperty("dctomoneda")
	private String dctomoneda;
	
	@JsonProperty("dctomonto")
	private double dctomonto;
	
	@JsonProperty("nrocheque")
	private String nrocheque;
	
	@JsonProperty("pagomontopagadoultimo")
	private double pagomontopagadoultimo;
	
	@JsonProperty("dctomontoultimo")
	private double dctomontoultimo;
	
	@JsonProperty("codigointerno")
	private String codigointerno;
	
	@JsonProperty("deguiapublicada")
	private int deguiapublicada;
	
	@JsonProperty("tipofactura")
	private String tipofactura;
	
	@JsonProperty("codigoerpproveedor")
	private String codigoerpproveedor;
	
	@JsonProperty("codigosociedad")
	private String codigosociedad;
	
	@JsonProperty("de_impuesto1")
	private double de_impuesto1;
	
	@JsonProperty("de_impuesto2")
	private double de_impuesto2;
	
	@JsonProperty("de_impuesto3")
	private double de_impuesto3;
	
	@JsonProperty("de_descuento")
	private double de_descuento;
	
	@JsonProperty("de_importereferencial")
	private double de_importereferencial;
	
	@JsonProperty("de_subtotalcomprobante")
	private double de_subtotalcomprobante;
	
	@JsonProperty("de_totalcomprobante")
	private double de_totalcomprobante;
	
	@JsonProperty("de_montopagadoultimo")
	private double de_montopagadoultimo;
	
	@JsonProperty("de_dctomontooultimo")
	private double de_dctomontooultimo;
	
	@JsonProperty("idestadobbva")
	private double idestadobbva;
	
	@JsonProperty("fechavencimientobbva")
	private double fechavencimientobbva;
	
	@JsonProperty("de_importepagobbva")
	private double de_importepagobbva;
	
	@JsonProperty("fechapagobbva")
	private double fechapagobbva;
	
	@JsonProperty("idindicadorimpuesto")
	private int idindicadorimpuesto;
	
	@JsonProperty("indicadorimpuesto")
	private String indicadorimpuesto;
	
	@JsonProperty("opregfac")
	private String opregfac;
	
	@JsonProperty("coderp")
	private String coderp;
	
	@JsonProperty("coderror")
	private String coderror;
	
	@JsonProperty("fechadocumentoret")
	private String fechadocumentoret;
	
	@JsonProperty("descerror")
	private String descerror;
	
	@JsonProperty("tipoemision")
	private String tipoemision;
	
	@JsonProperty("porcentajeimpuesto")
	private double porcentajeimpuesto;
	
	@JsonProperty("detraccion")
	private int detraccion;
	
	@JsonProperty("idbienservicio")
	private int idbienservicio;
	
	@JsonProperty("codigobienservicio")
	private String codigobienservicio;
	
	@JsonProperty("desc_bienservicio")
	private String desc_bienservicio;
	
	@JsonProperty("porc_detraccion")
	private String porc_detraccion;
	
	@JsonProperty("idcondicionpago")
	private String idcondicionpago;
	
	@JsonProperty("desc_condicionpago")
	private String desc_condicionpago;
	
	@JsonProperty("llaveerp")
	private String llaveerp;

	
	/////////////////////////////////////////////
	
	public int getIdcomprobantepago() {
		return idcomprobantepago;
	}

	public void setIdcomprobantepago(int idcomprobantepago) {
		this.idcomprobantepago = idcomprobantepago;
	}

	public String getNum_comprobantepago() {
		return num_comprobantepago;
	}

	public void setNum_comprobantepago(String num_comprobantepago) {
		this.num_comprobantepago = num_comprobantepago;
	}

	public int getIdusuarioproveedor() {
		return idusuarioproveedor;
	}

	public void setIdusuarioproveedor(int idusuarioproveedor) {
		this.idusuarioproveedor = idusuarioproveedor;
	}

	public int getIdorganizacioncompradora() {
		return idorganizacioncompradora;
	}

	public void setIdorganizacioncompradora(int idorganizacioncompradora) {
		this.idorganizacioncompradora = idorganizacioncompradora;
	}

	public int getIdorganizacionproveedora() {
		return idorganizacionproveedora;
	}

	public void setIdorganizacionproveedora(int idorganizacionproveedora) {
		this.idorganizacionproveedora = idorganizacionproveedora;
	}

	public String getRucproveedor() {
		return rucproveedor;
	}

	public void setRucproveedor(String rucproveedor) {
		this.rucproveedor = rucproveedor;
	}

	public String getRuccomprador() {
		return ruccomprador;
	}

	public void setRuccomprador(String ruccomprador) {
		this.ruccomprador = ruccomprador;
	}

	public int getIdestado() {
		return idestado;
	}

	public void setIdestado(int idestado) {
		this.idestado = idestado;
	}

	public int getIdmoneda() {
		return idmoneda;
	}

	public void setIdmoneda(int idmoneda) {
		this.idmoneda = idmoneda;
	}

	public String getEstado_comprobantepago() {
		return estado_comprobantepago;
	}

	public void setEstado_comprobantepago(String estado_comprobantepago) {
		this.estado_comprobantepago = estado_comprobantepago;
	}

	public String getFlag_plazopago() {
		return flag_plazopago;
	}

	public void setFlag_plazopago(String flag_plazopago) {
		this.flag_plazopago = flag_plazopago;
	}

	public String getFlag_registroeliminado() {
		return flag_registroeliminado;
	}

	public void setFlag_registroeliminado(String flag_registroeliminado) {
		this.flag_registroeliminado = flag_registroeliminado;
	}

	public String getFlag_origencomprobantepago() {
		return flag_origencomprobantepago;
	}

	public void setFlag_origencomprobantepago(String flag_origencomprobantepago) {
		this.flag_origencomprobantepago = flag_origencomprobantepago;
	}

	public String getFlag_origencreacion() {
		return flag_origencreacion;
	}

	public void setFlag_origencreacion(String flag_origencreacion) {
		this.flag_origencreacion = flag_origencreacion;
	}

	public int getIn_idtipodocumento() {
		return in_idtipodocumento;
	}

	public void setIn_idtipodocumento(int in_idtipodocumento) {
		this.in_idtipodocumento = in_idtipodocumento;
	}

	public String getCh_idtipodocumento() {
		return ch_idtipodocumento;
	}

	public void setCh_idtipodocumento(String ch_idtipodocumento) {
		this.ch_idtipodocumento = ch_idtipodocumento;
	}

	public int getIdguia() {
		return idguia;
	}

	public void setIdguia(int idguia) {
		this.idguia = idguia;
	}

	public int getIdoc() {
		return idoc;
	}

	public void setIdoc(int idoc) {
		this.idoc = idoc;
	}

	public int getIdusuariocreacion() {
		return idusuariocreacion;
	}

	public void setIdusuariocreacion(int idusuariocreacion) {
		this.idusuariocreacion = idusuariocreacion;
	}

	public int getIdusuariomodificacion() {
		return idusuariomodificacion;
	}

	public void setIdusuariomodificacion(int idusuariomodificacion) {
		this.idusuariomodificacion = idusuariomodificacion;
	}

	public int getIdorganizacioncreacion() {
		return idorganizacioncreacion;
	}

	public void setIdorganizacioncreacion(int idorganizacioncreacion) {
		this.idorganizacioncreacion = idorganizacioncreacion;
	}

	public int getIdorganizacionmodificacion() {
		return idorganizacionmodificacion;
	}

	public void setIdorganizacionmodificacion(int idorganizacionmodificacion) {
		this.idorganizacionmodificacion = idorganizacionmodificacion;
	}

	public String getRazonsocialproveedora() {
		return razonsocialproveedora;
	}

	public void setRazonsocialproveedora(String razonsocialproveedora) {
		this.razonsocialproveedora = razonsocialproveedora;
	}

	public String getRazonsocialcompradora() {
		return razonsocialcompradora;
	}

	public void setRazonsocialcompradora(String razonsocialcompradora) {
		this.razonsocialcompradora = razonsocialcompradora;
	}

	public String getMonedacomprobantepago() {
		return monedacomprobantepago;
	}

	public void setMonedacomprobantepago(String monedacomprobantepago) {
		this.monedacomprobantepago = monedacomprobantepago;
	}

	public String getFlagenvioguia() {
		return flagenvioguia;
	}

	public void setFlagenvioguia(String flagenvioguia) {
		this.flagenvioguia = flagenvioguia;
	}

	public String getFechaprogpagocomprobante() {
		return fechaprogpagocomprobante;
	}

	public void setFechaprogpagocomprobante(String fechaprogpagocomprobante) {
		this.fechaprogpagocomprobante = fechaprogpagocomprobante;
	}

	public String getFechapagocomprobante() {
		return fechapagocomprobante;
	}

	public void setFechapagocomprobante(String fechapagocomprobante) {
		this.fechapagocomprobante = fechapagocomprobante;
	}

	public String getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(String fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public String getFecharegistro() {
		return fecharegistro;
	}

	public void setFecharegistro(String fecharegistro) {
		this.fecharegistro = fecharegistro;
	}

	public String getFechaemision() {
		return fechaemision;
	}

	public void setFechaemision(String fechaemision) {
		this.fechaemision = fechaemision;
	}

	public String getFecharecepcioncomprobante() {
		return fecharecepcioncomprobante;
	}

	public void setFecharecepcioncomprobante(String fecharecepcioncomprobante) {
		this.fecharecepcioncomprobante = fecharecepcioncomprobante;
	}

	public String getFechavencimiento() {
		return fechavencimiento;
	}

	public void setFechavencimiento(String fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public String getFechaenvio() {
		return fechaenvio;
	}

	public void setFechaenvio(String fechaenvio) {
		this.fechaenvio = fechaenvio;
	}

	public String getFechacambioestado() {
		return fechacambioestado;
	}

	public void setFechacambioestado(String fechacambioestado) {
		this.fechacambioestado = fechacambioestado;
	}

	public String getObscomprobante() {
		return obscomprobante;
	}

	public void setObscomprobante(String obscomprobante) {
		this.obscomprobante = obscomprobante;
	}

	public String getObspagocomprobante() {
		return obspagocomprobante;
	}

	public void setObspagocomprobante(String obspagocomprobante) {
		this.obspagocomprobante = obspagocomprobante;
	}

	public double getImpuesto1() {
		return impuesto1;
	}

	public void setImpuesto1(double impuesto1) {
		this.impuesto1 = impuesto1;
	}

	public double getImpuesto2() {
		return impuesto2;
	}

	public void setImpuesto2(double impuesto2) {
		this.impuesto2 = impuesto2;
	}

	public double getImpuesto3() {
		return impuesto3;
	}

	public void setImpuesto3(double impuesto3) {
		this.impuesto3 = impuesto3;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getImportereferencial() {
		return importereferencial;
	}

	public void setImportereferencial(double importereferencial) {
		this.importereferencial = importereferencial;
	}

	public double getSubtotalcomprobante() {
		return subtotalcomprobante;
	}

	public void setSubtotalcomprobante(double subtotalcomprobante) {
		this.subtotalcomprobante = subtotalcomprobante;
	}

	public double getTotalcomprobante() {
		return totalcomprobante;
	}

	public void setTotalcomprobante(double totalcomprobante) {
		this.totalcomprobante = totalcomprobante;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getCondicionpago() {
		return condicionpago;
	}

	public void setCondicionpago(String condicionpago) {
		this.condicionpago = condicionpago;
	}

	public String getTiempoplazo() {
		return tiempoplazo;
	}

	public void setTiempoplazo(String tiempoplazo) {
		this.tiempoplazo = tiempoplazo;
	}

	public String getTipo_operacion() {
		return tipo_operacion;
	}

	public void setTipo_operacion(String tipo_operacion) {
		this.tipo_operacion = tipo_operacion;
	}

	public String getDocumentopago() {
		return documentopago;
	}

	public void setDocumentopago(String documentopago) {
		this.documentopago = documentopago;
	}

	public String getFormapago() {
		return formapago;
	}

	public void setFormapago(String formapago) {
		this.formapago = formapago;
	}

	public String getTipocomprobante() {
		return tipocomprobante;
	}

	public void setTipocomprobante(String tipocomprobante) {
		this.tipocomprobante = tipocomprobante;
	}

	public String getEstadocomprobante() {
		return estadocomprobante;
	}

	public void setEstadocomprobante(String estadocomprobante) {
		this.estadocomprobante = estadocomprobante;
	}

	public int getIdestadocomprobante() {
		return idestadocomprobante;
	}

	public void setIdestadocomprobante(int idestadocomprobante) {
		this.idestadocomprobante = idestadocomprobante;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getIdusuariocomprador() {
		return idusuariocomprador;
	}

	public void setIdusuariocomprador(int idusuariocomprador) {
		this.idusuariocomprador = idusuariocomprador;
	}

	public String getNumoc() {
		return numoc;
	}

	public void setNumoc(String numoc) {
		this.numoc = numoc;
	}

	public String getNumguia() {
		return numguia;
	}

	public void setNumguia(String numguia) {
		this.numguia = numguia;
	}

	public String getMontocomprobante() {
		return montocomprobante;
	}

	public void setMontocomprobante(String montocomprobante) {
		this.montocomprobante = montocomprobante;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String getPagotipodocumento() {
		return pagotipodocumento;
	}

	public void setPagotipodocumento(String pagotipodocumento) {
		this.pagotipodocumento = pagotipodocumento;
	}

	public String getPagonrodocumento() {
		return pagonrodocumento;
	}

	public void setPagonrodocumento(String pagonrodocumento) {
		this.pagonrodocumento = pagonrodocumento;
	}

	public String getPagomoneda() {
		return pagomoneda;
	}

	public void setPagomoneda(String pagomoneda) {
		this.pagomoneda = pagomoneda;
	}

	public double getPagomontopagado() {
		return pagomontopagado;
	}

	public void setPagomontopagado(double pagomontopagado) {
		this.pagomontopagado = pagomontopagado;
	}

	public String getPagobanco() {
		return pagobanco;
	}

	public void setPagobanco(String pagobanco) {
		this.pagobanco = pagobanco;
	}

	public String getDctotipodocumento() {
		return dctotipodocumento;
	}

	public void setDctotipodocumento(String dctotipodocumento) {
		this.dctotipodocumento = dctotipodocumento;
	}

	public String getDctonrodocumento() {
		return dctonrodocumento;
	}

	public void setDctonrodocumento(String dctonrodocumento) {
		this.dctonrodocumento = dctonrodocumento;
	}

	public String getDctomoneda() {
		return dctomoneda;
	}

	public void setDctomoneda(String dctomoneda) {
		this.dctomoneda = dctomoneda;
	}

	public double getDctomonto() {
		return dctomonto;
	}

	public void setDctomonto(double dctomonto) {
		this.dctomonto = dctomonto;
	}

	public String getNrocheque() {
		return nrocheque;
	}

	public void setNrocheque(String nrocheque) {
		this.nrocheque = nrocheque;
	}

	public double getPagomontopagadoultimo() {
		return pagomontopagadoultimo;
	}

	public void setPagomontopagadoultimo(double pagomontopagadoultimo) {
		this.pagomontopagadoultimo = pagomontopagadoultimo;
	}

	public double getDctomontoultimo() {
		return dctomontoultimo;
	}

	public void setDctomontoultimo(double dctomontoultimo) {
		this.dctomontoultimo = dctomontoultimo;
	}

	public String getCodigointerno() {
		return codigointerno;
	}

	public void setCodigointerno(String codigointerno) {
		this.codigointerno = codigointerno;
	}

	public int getDeguiapublicada() {
		return deguiapublicada;
	}

	public void setDeguiapublicada(int deguiapublicada) {
		this.deguiapublicada = deguiapublicada;
	}

	public String getTipofactura() {
		return tipofactura;
	}

	public void setTipofactura(String tipofactura) {
		this.tipofactura = tipofactura;
	}

	public String getCodigoerpproveedor() {
		return codigoerpproveedor;
	}

	public void setCodigoerpproveedor(String codigoerpproveedor) {
		this.codigoerpproveedor = codigoerpproveedor;
	}

	public String getCodigosociedad() {
		return codigosociedad;
	}

	public void setCodigosociedad(String codigosociedad) {
		this.codigosociedad = codigosociedad;
	}

	public double getDe_impuesto1() {
		return de_impuesto1;
	}

	public void setDe_impuesto1(double de_impuesto1) {
		this.de_impuesto1 = de_impuesto1;
	}

	public double getDe_impuesto2() {
		return de_impuesto2;
	}

	public void setDe_impuesto2(double de_impuesto2) {
		this.de_impuesto2 = de_impuesto2;
	}

	public double getDe_impuesto3() {
		return de_impuesto3;
	}

	public void setDe_impuesto3(double de_impuesto3) {
		this.de_impuesto3 = de_impuesto3;
	}

	public double getDe_descuento() {
		return de_descuento;
	}

	public void setDe_descuento(double de_descuento) {
		this.de_descuento = de_descuento;
	}

	public double getDe_importereferencial() {
		return de_importereferencial;
	}

	public void setDe_importereferencial(double de_importereferencial) {
		this.de_importereferencial = de_importereferencial;
	}

	public double getDe_subtotalcomprobante() {
		return de_subtotalcomprobante;
	}

	public void setDe_subtotalcomprobante(double de_subtotalcomprobante) {
		this.de_subtotalcomprobante = de_subtotalcomprobante;
	}

	public double getDe_totalcomprobante() {
		return de_totalcomprobante;
	}

	public void setDe_totalcomprobante(double de_totalcomprobante) {
		this.de_totalcomprobante = de_totalcomprobante;
	}

	public double getDe_montopagadoultimo() {
		return de_montopagadoultimo;
	}

	public void setDe_montopagadoultimo(double de_montopagadoultimo) {
		this.de_montopagadoultimo = de_montopagadoultimo;
	}

	public double getDe_dctomontooultimo() {
		return de_dctomontooultimo;
	}

	public void setDe_dctomontooultimo(double de_dctomontooultimo) {
		this.de_dctomontooultimo = de_dctomontooultimo;
	}

	public double getIdestadobbva() {
		return idestadobbva;
	}

	public void setIdestadobbva(double idestadobbva) {
		this.idestadobbva = idestadobbva;
	}

	public double getFechavencimientobbva() {
		return fechavencimientobbva;
	}

	public void setFechavencimientobbva(double fechavencimientobbva) {
		this.fechavencimientobbva = fechavencimientobbva;
	}

	public double getDe_importepagobbva() {
		return de_importepagobbva;
	}

	public void setDe_importepagobbva(double de_importepagobbva) {
		this.de_importepagobbva = de_importepagobbva;
	}

	public double getFechapagobbva() {
		return fechapagobbva;
	}

	public void setFechapagobbva(double fechapagobbva) {
		this.fechapagobbva = fechapagobbva;
	}

	public int getIdindicadorimpuesto() {
		return idindicadorimpuesto;
	}

	public void setIdindicadorimpuesto(int idindicadorimpuesto) {
		this.idindicadorimpuesto = idindicadorimpuesto;
	}
	
	public String getIndicadorimpuesto() {
		return indicadorimpuesto;
	}

	public void setIndicadorimpuesto(String indicadorimpuesto) {
		this.indicadorimpuesto = indicadorimpuesto;
	}
	
	public String getOpregfac() {
		return opregfac;
	}

	public void setOpregfac(String opregfac) {
		this.opregfac = opregfac;
	}

	public String getCoderp() {
		return coderp;
	}

	public void setCoderp(String coderp) {
		this.coderp = coderp;
	}

	public String getCoderror() {
		return coderror;
	}

	public void setCoderror(String coderror) {
		this.coderror = coderror;
	}
	
	public String getFechadocumentoret() {
		return fechadocumentoret;
	}

	public void setFechadocumentoret(String fechadocumentoret) {
		this.fechadocumentoret = fechadocumentoret;
	}

	public String getDescerror() {
		return descerror;
	}

	public void setDescerror(String descerror) {
		this.descerror = descerror;
	}

	public String getTipoemision() {
		return tipoemision;
	}

	public void setTipoemision(String tipoemision) {
		this.tipoemision = tipoemision;
	}

	public double getPorcentajeimpuesto() {
		return porcentajeimpuesto;
	}

	public void setPorcentajeimpuesto(double porcentajeimpuesto) {
		this.porcentajeimpuesto = porcentajeimpuesto;
	}

	public int getDetraccion() {
		return detraccion;
	}

	public void setDetraccion(int detraccion) {
		this.detraccion = detraccion;
	}

	public int getIdbienservicio() {
		return idbienservicio;
	}

	public void setIdbienservicio(int idbienservicio) {
		this.idbienservicio = idbienservicio;
	}

	public String getCodigobienservicio() {
		return codigobienservicio;
	}

	public void setCodigobienservicio(String codigobienservicio) {
		this.codigobienservicio = codigobienservicio;
	}

	public String getDesc_bienservicio() {
		return desc_bienservicio;
	}

	public void setDesc_bienservicio(String desc_bienservicio) {
		this.desc_bienservicio = desc_bienservicio;
	}

	public String getPorc_detraccion() {
		return porc_detraccion;
	}

	public void setPorc_detraccion(String porc_detraccion) {
		this.porc_detraccion = porc_detraccion;
	}

	public String getIdcondicionpago() {
		return idcondicionpago;
	}

	public void setIdcondicionpago(String idcondicionpago) {
		this.idcondicionpago = idcondicionpago;
	}

	public String getDesc_condicionpago() {
		return desc_condicionpago;
	}

	public void setDesc_condicionpago(String desc_condicionpago) {
		this.desc_condicionpago = desc_condicionpago;
	}

	public String getLlaveerp() {
		return llaveerp;
	}

	public void setLlaveerp(String llaveerp) {
		this.llaveerp = llaveerp;
	}	
	
	/////////////////////////////////
	
}
