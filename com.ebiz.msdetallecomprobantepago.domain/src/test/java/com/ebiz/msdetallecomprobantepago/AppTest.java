package com.ebiz.msdetallecomprobantepago;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testIdComprobante(){
    	Comprobantepago comprobantepago = new Comprobantepago();
    	comprobantepago.setIdcomprobantepago(2); 
    	System.out.println("id: "+comprobantepago.getIdcomprobantepago());
    	assertEquals(comprobantepago.getIdcomprobantepago(), 2 );
    }
    
    public void testComprobanteCreate() {
    	Comprobantepago comprobantepago = new Comprobantepago();
        assertNotNull(comprobantepago);
      }    
}
