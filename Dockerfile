FROM frolvlad/alpine-oraclejdk8:slim
#VOLUME /tmp
#VOLUME /Apps/arca/ms-customer
ADD ./com.ebiz.msdetallecomprobantepago.rest/target/com.ebiz.msdetallecomprobantepago.rest-1.0-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
#EXPOSE 8080
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]

 