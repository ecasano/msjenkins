package com.ebiz.msdetallecomprobantepago.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ebiz.msdetallecomprobantepago.Comprobantepago;
import com.ebiz.msdetallecomprobantepago.DetallecomprobantepagoService;

@RestController()
@RequestMapping("/comprobanteDetalle")
public class DetallecomprobantepagoController {

	@Autowired
	private DetallecomprobantepagoService detallecomprobantepagoService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Comprobantepago detalleComprobantepago(@PathVariable("id") int id) {
		return detallecomprobantepagoService.detalleComprobantepago(id);
	}
}
