package com.ebiz.msdetallecomprobantepago.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.msdetallecomprobantepago.Comprobantepago;
import com.ebiz.msdetallecomprobantepago.DetallecomprobantepagoRepository;

@Repository
public class DetallecomprobantepagoRepositoryImpl implements DetallecomprobantepagoRepository{
	static Logger log = Logger.getLogger(DetallecomprobantepagoRepositoryImpl.class.getName());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public Comprobantepago detalleComprobantepago(int id) {
		String sQL = "select * from comprobante.t_comprobantepago where in_idcomprobantepago=?";
		//probar sin lista
		List<Comprobantepago> lComprobantepago = null;
		
		try{
			lComprobantepago = jdbcTemplate.query(sQL,new Object[] { id }, this::mapParam);
			
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
		
		return lComprobantepago.get(0);
	}
	
	public Comprobantepago mapParam(ResultSet rs, int i) throws SQLException {
				
		Comprobantepago comprobantepago = new Comprobantepago();
		
		comprobantepago.setIdcomprobantepago(rs.getInt("in_idcomprobantepago"));
		comprobantepago.setNum_comprobantepago(rs.getString("ch_numerocomprobantepago"));
		comprobantepago.setIdusuarioproveedor(rs.getInt("in_idusuarioproveedor"));
		comprobantepago.setIdorganizacioncompradora(rs.getInt("in_idorganizacioncompradora"));
		comprobantepago.setIdorganizacionproveedora(rs.getInt("in_idorganizacionproveedora"));
		comprobantepago.setRucproveedor(rs.getString("ch_rucproveedor"));
		comprobantepago.setRuccomprador(rs.getString("ch_ruccomprador"));
		comprobantepago.setIdestado(rs.getInt("in_idestado"));
		comprobantepago.setIdmoneda(rs.getInt("in_idmoneda"));
		comprobantepago.setEstado_comprobantepago(rs.getString("ch_estadocomprobantepago"));
		comprobantepago.setFlag_plazopago(rs.getString("ch_flagplazopago"));
		comprobantepago.setFlag_registroeliminado(rs.getString("ch_flagregistroeliminado"));
		comprobantepago.setFlag_origencomprobantepago(rs.getString("ch_flagorigencomprobantepago"));
		comprobantepago.setFlag_origencreacion(rs.getString("ch_flagorigencreacion"));		
		comprobantepago.setIn_idtipodocumento(rs.getInt("in_idtipodocumento"));
		comprobantepago.setCh_idtipodocumento(rs.getString("ch_idtipodocumento"));
		comprobantepago.setIdguia(rs.getInt("in_idguia"));
		comprobantepago.setIdoc(rs.getInt("in_idoc"));
		comprobantepago.setIdusuariocreacion(rs.getInt("in_idusuariocreacion"));
		comprobantepago.setIdusuariomodificacion(rs.getInt("in_idusuariomodificacion"));
		comprobantepago.setIdorganizacioncreacion(rs.getInt("in_idorganizacioncreacion"));
		comprobantepago.setIdorganizacionmodificacion(rs.getInt("in_idorganizacionmodificacion"));
		comprobantepago.setRazonsocialproveedora(rs.getString("vc_razonsocialproveedora"));
		comprobantepago.setRazonsocialcompradora(rs.getString("vc_razonsocialcompradora"));
		comprobantepago.setMonedacomprobantepago(rs.getString("ch_monedacomprobantepago"));
		comprobantepago.setFlagenvioguia(rs.getString("ch_flagenvioguia"));
		comprobantepago.setFechaprogpagocomprobante(rs.getString("ts_fechaprogpagocomprobantepag"));
		comprobantepago.setFechapagocomprobante(rs.getString("ts_fechapagocomprobantepago"));
		comprobantepago.setFechacreacion(rs.getString("ts_fechacreacion"));
		comprobantepago.setFecharegistro(rs.getString("ts_fecharegistro"));
		comprobantepago.setFechaemision(rs.getString("ts_fechaemision"));
		comprobantepago.setFecharecepcioncomprobante(rs.getString("ts_fecharecepcioncomprobantepa"));
		comprobantepago.setFechavencimiento(rs.getString("ts_fechavencimiento"));
		comprobantepago.setFechaenvio(rs.getString("ts_fechaenvio"));
		comprobantepago.setFechacambioestado(rs.getString("ts_fechacambioestado"));
		comprobantepago.setObscomprobante(rs.getString("vc_obscomprobantepago"));
		comprobantepago.setObspagocomprobante(rs.getString("vc_obspagocomprobantepago"));
		comprobantepago.setImpuesto1(rs.getDouble("fl_impuesto1"));
		comprobantepago.setImpuesto2(rs.getDouble("fl_impuesto2"));
		comprobantepago.setImpuesto3(rs.getDouble("fl_impuesto3"));
		comprobantepago.setDescuento(rs.getDouble("fl_descuento"));
		comprobantepago.setImportereferencial(rs.getDouble("fl_importereferencial"));
		comprobantepago.setSubtotalcomprobante(rs.getDouble("fl_subtotalcomprobantepago"));
		comprobantepago.setTotalcomprobante(rs.getDouble("fl_totalcomprobantepago"));
		comprobantepago.setCampo1(rs.getString("vc_campo1"));
		comprobantepago.setCampo2(rs.getString("vc_campo2"));
		comprobantepago.setCondicionpago(rs.getString("vc_condicionpago"));
		comprobantepago.setTiempoplazo(rs.getString("ch_tiempoplazo"));
		comprobantepago.setTipo_operacion(rs.getString("vc_tipooperacion"));
		comprobantepago.setDocumentopago(rs.getString("vc_documentopago"));
		comprobantepago.setFormapago(rs.getString("vc_formapago"));
		comprobantepago.setTipocomprobante(rs.getString("vc_tipocomprobante"));
		comprobantepago.setEstadocomprobante(rs.getString("ch_estadocomprobantepagocomp"));
		comprobantepago.setIdestadocomprobante(rs.getInt("in_idestadocomp"));
		comprobantepago.setVersion(rs.getInt("in_version"));
		comprobantepago.setIdusuariocomprador(rs.getInt("in_idusuariocomprador"));
		comprobantepago.setNumoc(rs.getString("vc_numoc"));
		comprobantepago.setNumguia(rs.getString("vc_numguia"));
		comprobantepago.setMontocomprobante(rs.getString("vc_montocomprobantepago"));
		comprobantepago.setLogo(rs.getString("vc_logo"));
		comprobantepago.setFirma(rs.getString("vc_firma"));
		comprobantepago.setPagotipodocumento(rs.getString("vc_pagotipodocumento"));
		comprobantepago.setPagonrodocumento(rs.getString("vc_pagonrodocumento"));
		comprobantepago.setPagomoneda(rs.getString("vc_pagomoneda"));
		comprobantepago.setPagomontopagado(rs.getDouble("fl_pagomontopagado"));
		comprobantepago.setPagobanco(rs.getString("vc_pagobanco"));
		comprobantepago.setDctotipodocumento(rs.getString("vc_dctotipodocumento"));
		comprobantepago.setDctonrodocumento(rs.getString("vc_dctonrodocumento"));
		comprobantepago.setDctomoneda(rs.getString("vc_dctomoneda"));
		comprobantepago.setDctomonto(rs.getDouble("fl_dctomonto"));
		comprobantepago.setNrocheque(rs.getString("vc_nrocheque"));
		comprobantepago.setPagomontopagadoultimo(rs.getDouble("fl_pagomontopagadoultimo"));
		comprobantepago.setDctomontoultimo(rs.getDouble("fl_dctomontoultimo"));
		comprobantepago.setCodigointerno(rs.getString("ch_codigointerno"));
		comprobantepago.setDeguiapublicada(rs.getInt("in_deguiapublicada"));
		comprobantepago.setTipofactura(rs.getString("vc_tipofactura"));
		comprobantepago.setCodigoerpproveedor(rs.getString("vc_codigoerpproveedor"));
		comprobantepago.setCodigosociedad(rs.getString("vc_codigosociedad"));
		comprobantepago.setDe_impuesto1(rs.getDouble("de_impuesto1"));
		comprobantepago.setDe_impuesto2(rs.getDouble("de_impuesto2"));
		comprobantepago.setDe_impuesto3(rs.getDouble("de_impuesto3"));
		comprobantepago.setDe_descuento(rs.getDouble("de_descuento"));
		comprobantepago.setDe_importereferencial(rs.getDouble("de_importereferencial"));
		comprobantepago.setDe_subtotalcomprobante(rs.getDouble("de_subtotalcomprobantepago"));
		comprobantepago.setDe_totalcomprobante(rs.getDouble("de_totalcomprobantepago"));
		comprobantepago.setDe_montopagadoultimo(rs.getDouble("de_pagomontopagadoultimo"));
		comprobantepago.setDe_dctomontooultimo(rs.getDouble("de_dctomontoultimo"));
		comprobantepago.setIdestadobbva(rs.getDouble("in_idestadobbva"));
		comprobantepago.setFechavencimientobbva(rs.getDouble("ts_fechavencimientobbva"));
		comprobantepago.setDe_importepagobbva(rs.getDouble("de_importepagobbva"));
		comprobantepago.setFechapagobbva(rs.getDouble("ts_fechapagobbva"));
		comprobantepago.setIdindicadorimpuesto(rs.getInt("in_idindicadorimpuesto"));
		comprobantepago.setIndicadorimpuesto(rs.getString("vc_indicadorimpuesto"));
		comprobantepago.setOpregfac(rs.getString("ch_opregfac"));
		comprobantepago.setCoderp(rs.getString("vc_codigoerp"));
		comprobantepago.setCoderror(rs.getString("vc_coderror"));
		comprobantepago.setFechadocumentoret(rs.getString("ts_fechadocumentoret"));
		comprobantepago.setDescerror(rs.getString("vc_descerror"));
		comprobantepago.setTipoemision(rs.getString("ch_tipoemision"));
		comprobantepago.setPorcentajeimpuesto(rs.getDouble("de_porcentajeimpuesto"));
		comprobantepago.setDetraccion(rs.getInt("in_detraccion"));
		comprobantepago.setIdbienservicio(rs.getInt("in_idbienservicio"));
		comprobantepago.setCodigobienservicio(rs.getString("vc_codigobienservicio"));
		comprobantepago.setDesc_bienservicio(rs.getString("vc_descripcionbienservicio"));
		comprobantepago.setPorc_detraccion(rs.getString("vc_porcentajedetraccion"));
		comprobantepago.setIdcondicionpago(rs.getString("vc_idcondicionpago"));
		comprobantepago.setDesc_condicionpago(rs.getString("vc_descripcioncondicionpago"));
		comprobantepago.setLlaveerp(rs.getString("vc_llaveerp"));				
		
		if(i>0){
			log.info("Niquititito");
			
		}
		
		return comprobantepago;
	}
}
