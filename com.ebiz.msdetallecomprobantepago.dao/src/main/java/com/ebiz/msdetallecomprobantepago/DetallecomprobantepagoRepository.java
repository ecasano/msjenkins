package com.ebiz.msdetallecomprobantepago;

import com.ebiz.msdetallecomprobantepago.Comprobantepago;

public interface DetallecomprobantepagoRepository {
	
	Comprobantepago detalleComprobantepago(int id);
}
